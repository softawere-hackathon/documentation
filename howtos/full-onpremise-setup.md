# Deploy SoftAWERE toolkit the easy way

Ensure you have the required dependencies (ubuntu):

```bash
apt install git docker.io docker-compose
```

Please ensure you have **docker-compose version 1.29.1** or above.

Clone the SoftAWERE repository.

```bash
git clone https://gitlab.com/softawere-hackathon/softawere.git
```

Configure the stack by editing the `.env` file, especially the token `GITL_RUNN_REGISTER_TOKEN` for your gitlab runner to register to your gitlab group and/or project. You can get this token, either for a group or a single project, in the section **Settings > CI/CD > Runners**.

Boot the stack

```bash
cd softawere && docker-compose up -d
```

Check the setup is working, running the appropriate script.

```bash
bash validate.sh
```

In there is any error, follow the instructions.

If everything is fine at this stage, you should fork the [template project](https://gitlab.com/softawere-hackathon/thisisatest) in your gitlab group, whether it's on gitlab.com or an on-premise instance of Gitlab.

By default you get a local grafana instance. Connect to your server IP address on port 3000 to setup the admin account. We also recommend you setup a reverse proxy like Nginx or Apache to provide a cleaner access to Grafana to your users.
