# Updating the documentation

This documentation is build with Honkit: [GitHub - honkit/honkit: HonKit is building beautiful books using Markdown - Fork of GitBook](https://github.com/honkit/honkit).

1. Install Honkit:  [Installation and Setup · HonKit Documentation](https://honkit.netlify.app/setup.html).
2. Run locally with `npx honkit serve`.
3. Open [http://localhost:4000/](http://localhost:4000/)

Edit documentation, Honkit shoud like reload, you changes.
