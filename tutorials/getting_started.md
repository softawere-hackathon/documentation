# Getting started

The easiest way to get started with having environmental impact metrics in your project's development process, it to use the SoftAWERE *publicly accessible* Gitlab Runner.

> ⚠ This _publicly accessible_ Gitlab Runner is _shared_  and used by all projects. This shared runner is considered safe enough for testing but comes without guarantee. It may execute code that you cannot trust.
>
> 💡 For any sensitive codebase, you should rather hosting your _own private_ SoftAWERE runner (see [Deploy the full setup (+Gitlab Runner)](https://softawere-hackathon.gitlab.io/documentation/howtos/full-onpremise-setup.html))

## 1. Join the Gitlab group

To be able to run CI/CD jobs in the group and benefit from the publicly accessible Gitlab runner, just [ask for it](https://gitlab.com/groups/softawere-hackathon/-/group_members/request_access) with your gitlab account signed in.

## 2. Create a new repository in the softawere-hackathon group

Create a new project, clicking on the [**"New Project"**](https://gitlab.com/projects/new?namespace_id=57159126) button.

Please name it **with a "test-" prefix** so we can distinguish between applications repositories and test repositories.

![new_project.png](new_project.png)

## 3. Disable "shared runners" on your project

By default the project is using Gitlab.com shared runners. You need to disable this, so your project only uses the SoftAWERE custom gitlab runner, including appropriate monitoring tools. Go to **Settings > CI/CD > Runners**.

![disable_shared_runners.png](disable_shared_runners.png)

## 4. Bootstrap your application test environment

Now let's set up your application to build or run tests using Gitlab.

Once your application build process or tests are working, we can move on to measuring the environmental impact (if you don't need an example, you can skip to Step 5 right away).

As an example, we are going to configure the build process of the [SDIA Website](https://gitlab.com/softawere-hackathon/sdia-website) (Gatsby static builder) on Gitlab.

### Setting up the Gitlab CI file.

You can read more about Gitlab configuration options (.gitlab-ci.yml) in [their official documentation](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html).

In our example, we keep things simple and only have a build stage (no tests).
We choose a basic docker image as the base that comes with NodeJS pre-installed.

```yaml
default:
  image: node:current-alpine3.16

stages:
  - build

build-website-job:
  stage: build
  cache:
    key: node-modules
    paths:
      - node_modules
  script:
    # commands here to build the website
```

Let's install our prerequisites using the ```.pre``` stage.

```yaml
default:
  image: node:current-alpine3.16

stages:
  - build

install-dependencies:
  stage: .pre
  script:
    - yarn install
    - echo "This job runs in the .pre stage, before all other stages."

build-website-job:
  stage: build
  cache:
    key: node-modules
    paths:
      - node_modules
  script:
    # commands here to build the website
```

And then let's run our build process for the website:

```yaml
default:
  image: node:current-alpine3.16

stages:
  - build

install-dependencies:
  stage: .pre
  script:
    - yarn install
    - echo "This job runs in the .pre stage, before all other stages."

build-website-job:
  stage: build
  cache:
    key: node-modules
    paths:
      - node_modules
  script:
    - yarn run build
```

That's it! Now we can add our code to measure the environmental impact of the build process (or if this is your own project, your tests or anything else that is being executed during the CI script).

## 6. Adding the measurement scripts to the gitlab-ci.yml

Now to measure the environmental impact we need to install some dependencies for the scripts that will run & capture the environmental performance metrics.

We do not need a lot:

* python (and some pip packages)
* curl

For the commands below you might have to change them *depending on the distribution of the base image you are using*.
For example for Alpine, the commands are `apk add` for Ubuntu its `apt-get install`. If you are already installing python as part of your own dependencies or if you are using a base image that contains python already, you might also be able to remove some duplication.

Please integrate the example below into your own `.gitlab-ci.yml` file, e.g. by amending your `before_scipt` and `script` commands.

```yaml
your-build-job:
  stage: build
  before_script:
    - apk add --no-cache python3 && ln -sf python3 /usr/bin/python
    - apk add curl
    - python3 -m ensurepip
    - pip3 install --no-cache --upgrade pip setuptools
    - curl -O https://gitlab.com/softawere-hackathon/softawere/-/raw/main/gitlab-metric-script/requirements-metrics.txt
    - pip install -r requirements-metrics.txt
```

Now inside your actual execution of either tests or builds, you can place some environmental variables to help with the measurement itself. `CI_JOB_COVERAGE` is an integer percentage value (0-100) you can submit as well as `CI_JOB_TEST_COUNT` to help our research (integer).

```yaml
your-build-job:
  stage: build
  before_script:
    # install dependencies for the measurement tools
    - apk add --no-cache python3 && ln -sf python3 /usr/bin/python
    - apk add curl
    - python3 -m ensurepip
    - pip3 install --no-cache --upgrade pip setuptools
    - curl -O https://gitlab.com/softawere-hackathon/softawere/-/raw/main/gitlab-metric-script/requirements-metrics.txt
    - pip install -r requirements-metrics.txt
  script:
    # --- Pass information to the measurement tool via environment variables --- #
    - export CI_JOB_SIZE=$(du -sk . | sed  's/\([0-9]*\).*$/\1/g')
    - export CI_JOB_AFTER_REQUIREMENTS_STARTED_AT=$(date +%s)

    #
    # - YOUR BUILD/TEST COMMANDS
    #

    # --- Pass information to the measurement tool via environment variables --- #
    # - export CI_JOB_COVERAGE=$(sed -n --regexp-extended --expression='s/TOTAL.*\s([0-9\%]{2,4})/\1/p' coverage.txt)
    # - export CI_JOB_TEST_COUNT=$(sed -n --regexp-extended --expression='s/\=+\s([0-9]+[^\s]).*/\1/p' test_output.txt)

    # --- Trigger the collection of measurement data and upload to NocoDB --- #
    - curl -s https://gitlab.com/softawere-hackathon/softawere/-/raw/main/gitlab-metric-script/get_metrics.py | python
```

Ensure it works, commit and push, then go to CI/CD > Pipelines to check for the job output.

## 7. Look and understand the results in the bottom of job log messages

The script will output 3 pieces of information for you:

* A link to a Grafana dashboard where you can see environmental impact of your pipeline over time
* A link to our central database (NocoDB) where the results of all the builds are stored for research
* An overview of environmental impact metrics for the job itself

If you want to understand what each of the metrics mean, have a look at the [explanation](../explanations/lca_based_environmental_criterias.md) for environmental impact metrics.


## Using the Gitlab Service approach to run complex applications

If you have a complex application, you can also package the application as a Docker image and ask Gitlab to boot the application as a service during the CI/CD process.

1. Create an extra repository (in addition to the one for the tests), in the group, name it after your application.
2. Write a Dockerfile for your application, don't forget about the `PORT` instruction to tell about the TCP port number the application will be listening to. Gitlab CI need it to know how to expose the application as a service in je CI job. [Here is an example application repository](https://gitlab.com/softawere-hackathon/python3-default-fastapi-helloworld).
3. Follow the README of the [sample repository](https://gitlab.com/softawere-hackathon/python3-default-fastapi-helloworld) to locally build and push to the gitlab registry.

### Edit gitlab-ci.yml and configure the job to run your app as a container/service

To run an application as a [Gitlab service](https://docs.gitlab.com/ee/ci/services/) and run your tests in the main job, ensure you have a `services` configuration section in `gitlab-ci.yml` referencing the image you created before. Like this:

```yaml
services:
  ## use gitlab registry and add uri of the image
  - name: registry.gitlab.com/softawere-hackathon/python3-default-fastapi-helloworld:latest 
    alias: myapppython3-default-fastapi-helloworld
```

If your application includes unit tests for example, you could also get rid of this part and just run them in the main job after a clone of the appropriate branch you want to test.