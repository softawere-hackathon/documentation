The SoftAWERE complete test bed looks like this.

![Architecture of the SoftAWERE test bed](architecture.jpg)

The project is a framework, composed of :

- [Boagent](https://github.com/Boavizta/boagent), itself using:
    - [BoaviztAPI](https://github.com/Boavizta/boaviztapi) to give environmental footprint impact factors, compute final impact based on usage values and hardware specifications
    - [Scaphandre](https://github.com/hubblo-org/scaphandre/) to get power and energy consumption metrics of the machine and its running processes
    - Boagent's main role is to scrape the local machine's hardware specifications and query the right data to BoaviztAPI and Scaphandre
- [Prometheus](https://prometheus.io) to collect and store metrics
- Prometheus's [pushgateway](https://github.com/prometheus/pushgateway) to receive metrics from
- [Gitlab Runner](https://docs.gitlab.com/runner/) to allow CI/CD jobs to run on the machine and be tested and measured regarding their environmental footprint
- [A sample gitlab project](https://gitlab.com/softawere-hackathon/thisisatest) that includes scripts to gather the impact metrics from the current CI job and send those metrics to the push gateway
- [container-stats](https://github.com/bpetit/container-stats/) : a very simple script, running in a container, that exposes docker-stats metrics as a prometheus exporter, from the host (this is temporary and should be replaced by a cleaner implementation of resources consumption gathering in either scaphandre or boagent)
