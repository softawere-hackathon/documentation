# Life Cycle Assessment environmental impact criterias

## Impact criterias

### GWP - Global Warming Potential

Reported in kg CO2 eq.

### ADPe - (element) Abiotic Resources Depletion

<p align="center">
    <img src="isa_mine.jpg" alt="Picture of Mt Isa mine" caption="“Mt Isa mines. One of the big holes adjacent to the city.” by denisbin, CC BY-ND 2.0." height="300"/>
</p>

Reported in kg Sb (antimony) eq.

### PE - Primary Energy

Reported in Mega Joules.

## Life Cycle steps

### Raw materials extraction

Grouped in the named **embodied** impacts metrics.

### Manufacturing

Grouped in the named **embodied** impacts metrics.

### Transport/installation

Grouped in the named **embodied** impacts metrics.

### Usage

Grouped in the named **operational** impacts metrics.

### End of life

Grouped in the named **embodied** impacts metrics.
