# SoftAWERE

This project is about helping developers to make environmentally friendly choices, regarding their stacks and applications.

This project is born from a collaboration between the [SDIA](https://sdialliance.org) and [Boavizta](https://boavizta.org/en).

## Principle

You estimate the environmental impacts of your application during a the execution of test scenario in Gitlab CI.

![The pitch](use-case-view.excalidraw.png)

### Softaware test bench

The Softaware test bench is the component that allow estimating the environmental impacts of your application during the tests.

Requirements:

1. The scenario is written as a Gitlab CI script. It has to be  a **black box** test (i.e. run from _outside_ your application).
2. Your application under test has to be  **containerized**. It will be exposed as a Gitlab-ci service and tested from _outside_.
3. Your need to use a specific **Gitlab runner**. This runner runs on a **physical machine** (no VM). This is necessary to retrieve environmental metrics (like power consumption) during the test.

![High level view](high-level-view.excalidraw.png)

You can use the provided docker-compose to configure and register a specific Gitlab runner that will allow measurement.

## Documentation

Here's how you could navigate this documentation :

* [Tutorials](tutorials/README.md) allow you to walkthrough the solution with guidance. This could be a first approach to discover and learn about what's provided.
* [How-to guides](howtos/README.md) are the right section for you if you want to perform a special setup or run this project in a specific way and look for a well defined procedure.
* [Explanations](explanations/README.md) are here to provide deeper clarifications about what's going under de hood on when you use this project.
* [References](references/README.md) are options, features and flags references, for those who want to expertedly tweak tiny bits of configuration to get a fine tuned setup.

### Contributors and sponsors

<p align="center">
    <a href="https://sdialliance.org"><img src="sdia_logo.png" height="70"/></a>
</p>

<p align="center">
    <a href="https://boavizta.org/en"><img src="boavizta_logo.png" height="90"/></a>
</p>

<p align="center">
    <a href="https://helio.exchange"><img src="helio_logo.svg" height="70"/></a>
</p>

<p align="center">
    <a href="https://hubblo.org"><img src="https://hubblo.org/hubblo_logo_complet.svg" height="65"/></a>
</p>
