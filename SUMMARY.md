# Summary

* [Introduction](README.md)
* [Tutorials](tutorials/README.md)
    * [Getting started](tutorials/getting_started.md)
* [How-to guides](howtos/README.md)
    * [Deploy the full setup (+Gitlab Runner)](howtos/full-onpremise-setup.md)
    * [Updating the documentation](howtos/updating-the-documentation.md)
* [Explanations](explanations/README.md)
    * [Technical architecture](explanations/technical_architecture.md)
    * [LCA based impact criterias and life cycle steps](explanations/lca_based_environmental_criterias.md)
    * [Pre-requesites for on-premise installation](explanations/pre-requesites)
* [References](references/README.md)
